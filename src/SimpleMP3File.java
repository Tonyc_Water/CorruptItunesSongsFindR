
public class SimpleMP3File {
	private String title;
	private String artist;
	private String absolutePath;
	private boolean isLikelyDuplicate;

	public SimpleMP3File(String title, String artist, String absolutePath) {
		this.title = title;
		this.artist = artist;
		this.absolutePath = absolutePath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public boolean isLikelyDuplicate() {
		return isLikelyDuplicate;
	}

	public void setLikelyDuplicate(boolean isLikelyDuplicate) {
		this.isLikelyDuplicate = isLikelyDuplicate;
	}

}
