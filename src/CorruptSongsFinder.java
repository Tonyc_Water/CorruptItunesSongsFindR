
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.ID3v24Tag;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class CorruptSongsFinder {
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	private static int deletedEmptyDirectoriesCounter = 0;
	private static ArrayList<String> toDeletelist = new ArrayList<String>();
	private static ArrayList<SimpleMP3File> allSimpleMP3Files;
	private static long canceledProgress = 0;

	private static Long lengthOfTask = 1L;
	private static Long current = 0L;
	private static String statMessage = "";
	private static boolean synchronizeSwitchOn;
	private static boolean userCanceled;
	private static long copyTimeNeededInMillis;
	private static double copyAmountInKiloBytes;

	private static boolean emptyFolderDeleteJobDone;

	private static ArrayList<File> corruptSoundFiles;
	private static long startTimeMillis;
	private static String itunesMusicLibraryXMLContent;

	public static int getDeletedEmptyDirectoriesCounter() {
		return deletedEmptyDirectoriesCounter;
	}

	public static void setDeletedEmptyDirectoriesCounter(int deletedEmptyDirectoriesCounter) {
		CorruptSongsFinder.deletedEmptyDirectoriesCounter = deletedEmptyDirectoriesCounter;
	}

	public static void findDuplicates(File directory) {
		resetVariables();
		allSimpleMP3Files = new ArrayList<SimpleMP3File>();
		corruptSoundFiles = new ArrayList<File>();
		findAllCorruptSoundFilesCascading(new ArrayList<File>(getFilesArrayListFromDirectoryFile(directory)),
				directory);
		current = lengthOfTask;
	}

	public static void resetVariables() {
		statMessage = "";
		current = 0L;
		toDeletelist.clear();
		userCanceled = false;
		canceledProgress = 0;
		emptyFolderDeleteJobDone = false;
		copyTimeNeededInMillis = 0;
		copyAmountInKiloBytes = 0;
	}

	private static ArrayList<File> getFilesArrayListFromDirectoryFile(File file) {
		String[] currentPathFileNames = file.list();
		ArrayList<File> resultList = new ArrayList<File>();
		if (!file.isDirectory() || currentPathFileNames == null) {
			return null;
		}
		for (String currentFilePath : currentPathFileNames) {
			if (!currentFilePath.toLowerCase().endsWith(".mp3") && currentFilePath.contains(".")) {
				continue;
			}
			resultList.add(new File(file.getAbsolutePath() + FILE_SEPARATOR + currentFilePath));
		}
		return resultList;
	}

	private static void findAllCorruptSoundFilesCascading(ArrayList<File> allMasterFilesCurrentLevel,
			File currentLevelMasterDir) {
		if (userCanceled) {
			return;
		}
		for (File currentFile : allMasterFilesCurrentLevel) {
			if (userCanceled) {
				return;
			}
			if (currentFile.isDirectory()) {
				ArrayList<File> filesArrayListFromDirectoryFile = getFilesArrayListFromDirectoryFile(currentFile);
				if (filesArrayListFromDirectoryFile != null) {
					findAllCorruptSoundFilesCascading(filesArrayListFromDirectoryFile, currentFile);
				}
			} else {
				// if (current > 1000) {
				// return;
				// }

				if (!currentFile.getName().toLowerCase().endsWith(".mp3")
						&& !currentFile.getName().toLowerCase().endsWith(".m4a")) {
					continue;
				}
				if (isCorrupt(currentFile)) {
					corruptSoundFiles.add(currentFile);
				}

				statMessage = (current + 1) + ". of " + lengthOfTask + " MP3 / M4A files. Current file: '"
						+ currentFile.getAbsolutePath() + "'";
				current++;
			}
		}
	}

	private static boolean isCorrupt(File file) {
		if (itunesMusicLibraryXMLContent == null) {
			itunesMusicLibraryXMLContent = xmlFile2String(System.getProperty("user.home") + FILE_SEPARATOR + "Music"
					+ FILE_SEPARATOR + "iTunes" + FILE_SEPARATOR + "iTunes Music Library.xml");
		}

		if (file.getName().toLowerCase().endsWith(".m4a")) {
			if (!itunesMusicLibraryXMLContent.contains(file.getName().substring(0, file.getName().length() - 4))) {
				System.out
						.println("corrupt file! title=" + file.getName() + "\t\tfile path: " + file.getAbsolutePath());
				return true;
			}
		}

		MP3File audioFile = null;
		ID3v24Tag tag = null;
		try {
			audioFile = (MP3File) AudioFileIO.read(file);
			if (audioFile.hasID3v2Tag()) {
				ID3v24Tag t = audioFile.getID3v2TagAsv24();
				tag = t;
			} else {
				return false;
			}
		} catch (CannotReadException ex) {
			Logger.getLogger(CorruptSongsFinder.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(CorruptSongsFinder.class.getName()).log(Level.SEVERE, null, ex);
		} catch (TagException ex) {
			Logger.getLogger(CorruptSongsFinder.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ReadOnlyFileException ex) {
			Logger.getLogger(CorruptSongsFinder.class.getName()).log(Level.SEVERE, null, ex);
		} catch (InvalidAudioFrameException ex) {
			Logger.getLogger(CorruptSongsFinder.class.getName()).log(Level.SEVERE, null, ex);
		}
		if (tag == null) {
			return false;
		}
		String title = "";
		String artist = "";

		try {
			title = tag.getFirstField(FieldKey.TITLE).toString().replace("Text=", "").replace("\"", "").replace(";", "")
					.trim();
			artist = tag.getFirstField(FieldKey.ARTIST).toString().replace("Text=", "").replace("\"", "").replace(";",
					"");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		if (!itunesMusicLibraryXMLContent.contains(title)) {
			System.out.println("corrupt file! title=" + title + "\t\tfile path: " + file.getAbsolutePath());
			return true;
		}
		return false;
	}

	public static String xmlFile2String(String filePath) {
		String st = null;
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			InputSource is = new InputSource(filePath);
			Document document = docBuilderFactory.newDocumentBuilder().parse(is);
			StringWriter sw = new StringWriter();
			Transformer serializer = TransformerFactory.newInstance().newTransformer();
			serializer.transform(new DOMSource(document), new StreamResult(sw));
			st = sw.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return st;
		return st.replace("%20", " ").replace("%21", "!").replace("%22", "\"").replace("%23", "#").replace("%24", "$")
				.replace("%25", "%").replace("%26", "&").replace("%27", "'").replace("%2D", "-")
				.replace("%E2%80%9A", ",").replace("&amp;", "&").replace("&#38;", "&").replace("%E2%84", "™")
				.replace("%C3%88", "È").replace("%C3%89", "É").replace("%C3%8A", "Ê").replace("%C3%A8", "è")
				.replace("%C3%A9", "é").replace("%C3%AA", "ê").replace("%5B", "[").replace("%5D;", "]")
				.replace("%C3%A4", "ä").replace("%C3%84", "Ä").replace("%C3%B6", "ö").replace("%C3%96", "Ö")
				.replace("%C3%BC", "ü").replace("%C3%9C", "Ü").replace("%C3%9F", "ß").replace("\"", "");

		// StringBuilder contentBuilder = new StringBuilder();
		// try (Stream<String> stream = Files.lines(Paths.get(filePath),
		// StandardCharsets.UTF_8)) {
		// stream.forEach(s -> contentBuilder.append(s).append("\n"));
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// return contentBuilder.toString();

	}

	private static String cleanTextContent(String text) {
		// strips off all non-ASCII characters
		text = text.replaceAll("[^\\x00-\\x7F]", "");

		// erases all the ASCII control characters
		text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

		// removes non-printable characters from Unicode
		text = text.replaceAll("\\p{C}", "");

		return text.trim();
	}

	public static void printAllDuplicateMP3Files() {
		for (File currFile : corruptSoundFiles) {
			// System.out.println(currFile.getAbsolutePath());
		}
	}

	// Progressbar-Code

	/**
	 * Called from Frontend to start the task.
	 */
	void go(File directory) {
		current = 0L;
		final SwingWorker worker = new SwingWorker() {
			public Object construct() {
				return new ActualTask(directory);
			}
		};
		worker.start();
	}

	/**
	 * Called from Frontend to find out how much work needs to be done.
	 */
	long getLengthOfTask() {
		return lengthOfTask;
	}

	/**
	 * Called from Frontend to find out how much has been done.
	 */
	public static long getCurrent() {
		return current;
	}

	static void setCurrent(long newCurrent) {
		current = newCurrent;
	}

	void stop() {
		statMessage = "Canceled by user.";
		current = lengthOfTask;
	}

	/**
	 * Called from Frontend to find out if the task has completed.
	 */
	boolean done() {
		if (current >= lengthOfTask) {
			if (!synchronizeSwitchOn) {
				return emptyFolderDeleteJobDone;
			}
			return true;
		} else {
			return false;
		}
	}

	String getMessage() {
		return statMessage;
	}

	/**
	 * The actual long running task. This runs in a SwingWorker thread.
	 */
	class ActualTask {
		ActualTask(File directory) {
			// lengthOfTask = countFilesInDirectory(masterDir);
			startTimeMillis = System.currentTimeMillis();
			findDuplicates(directory);
			if (current > lengthOfTask) {
				current = lengthOfTask;
			}
			// statMessage = "Completed. " + current + " out of " + lengthOfTask + " files
			// processed.";
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
			}
		}
	}

	// Progressbar-Code End

	/**
	 * Count files in a directory (including files in all subdirectories)
	 * 
	 * @param directory
	 *            the directory to start in
	 * @return the total number of mp3 files
	 */
	public static int countMP3FilesInDirectory(File directory) {
		int count = 0;
		for (File file : directory.listFiles()) {
			if (file.getName().toLowerCase().endsWith(".mp3") || file.getName().toLowerCase().endsWith(".m4a")) {
				count++;
			}
			if (file.isDirectory()) {
				count += countMP3FilesInDirectory(file);
			}
		}
		return count;
	}

	public static int countTotalFoldersInDirectory(File directory) {
		int count = 0;
		File[] folderList = directory.listFiles(new FileFilter() {
			@Override
			public boolean accept(File f) {
				return f.isDirectory();
			}
		});

		if (folderList == null || folderList.length < 1) {
			return 0;
		}
		for (File file : folderList) {
			// System.out.println(file.getAbsolutePath());
			count++;
			count += countTotalFoldersInDirectory(file);
		}
		return count;

	}

	public static String getTimeLeftString() {
		if (current == 0) {
			return "";
		}
		Long currentTimeNeededMillis = System.currentTimeMillis() - startTimeMillis;

		Long timeNeededForAllMillis = (long) ((currentTimeNeededMillis.doubleValue() / current.doubleValue())
				* lengthOfTask.doubleValue());
		Long timeLeftSeconds = (timeNeededForAllMillis - currentTimeNeededMillis) / 1000;
		Long timeLeftOnlySeconds = timeLeftSeconds % 60;

		// return "(time left: " + timeLeftSeconds + " secs)";
		Long timeLeftOnlyMinutes = ((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) % 60;
		Long timeLeftOnlyHours = 0L;
		if (((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) > 60) {
			timeLeftOnlyHours = (((timeLeftSeconds - (timeLeftSeconds % 60)) / 60) - timeLeftOnlyMinutes) / 60;
		}
		String timeLeftOnlyHoursString = timeLeftOnlyHours > 9 ? timeLeftOnlyHours.toString() : "0" + timeLeftOnlyHours;
		String timeLeftOnlyMinutesString = timeLeftOnlyMinutes > 9 ? timeLeftOnlyMinutes.toString()
				: "0" + timeLeftOnlyMinutes;
		String timeLeftOnlySecondsString = timeLeftOnlySeconds > 9 ? timeLeftOnlySeconds.toString()
				: "0" + timeLeftOnlySeconds;

		return "(time left: " + timeLeftOnlyHoursString + ":" + timeLeftOnlyMinutesString + ":"
				+ timeLeftOnlySecondsString + ")";
	}

	public static void setSynchronizeSwitchOn(boolean synchronizeSwitchOn) {
		CorruptSongsFinder.synchronizeSwitchOn = synchronizeSwitchOn;
	}

	public static boolean isSynchronizeSwitchOn() {
		return synchronizeSwitchOn;
	}

	public static void setLengthOfTask(long lengthOfTask) {
		CorruptSongsFinder.lengthOfTask = lengthOfTask;
	}

	public static void setUserCanceled(boolean userCanceled) {
		CorruptSongsFinder.userCanceled = userCanceled;
	}

	public static boolean isUserCanceled() {
		return userCanceled;
	}

	public static long getCanceledProgress() {
		return canceledProgress;
	}

	public static void setCanceledProgress(long canceledProgress) {
		CorruptSongsFinder.canceledProgress = canceledProgress;
	}

	public static boolean isEmptyFolderDeleteJobDone() {
		return emptyFolderDeleteJobDone;
	}

	public static double getCurrentAverageCopySpeedInKiloBytesPerSecond() {
		return (double) (copyAmountInKiloBytes / (((double) copyTimeNeededInMillis) / 1000.0));
	}

	public static ArrayList<File> getCorruptSoundFiles() {
		return corruptSoundFiles;
	}

}
